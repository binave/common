package org.binave.common.bytecode;

import java.util.List;

/**
 * @author by bin jin on 2017/5/4.
 * @since 1.8
 */
public interface iBase {

    int getI();

    long getL();

    List<Ab> getAbList();
}
