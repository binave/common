package org.binave.common.bytecode;

/**
 * @author by bin jin on 2017/5/3.
 * @since 1.8
 */
public interface IHello {

    void MethodA();

    void MethodB();

    void Abs();
}
